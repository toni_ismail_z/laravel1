<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Admin extends Controller
{

    public function index()
    {
        return view('admin.index');
    }

    public function tables()
    {
        return view('admin.table');
    }

    public function data_tables()
    {
        return view('admin.data-table');
    }

}
